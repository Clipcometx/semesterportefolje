﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Drawing;

namespace WpfApp1.Model
{
    internal class ImageProcceingModel
    {
        private Image<Gray, byte> backgroundImage;

        public Bitmap CannyEdgeDetection(Mat source, double thresh = 20, double threshlinking = 50, int matrix = 5)
        {
            Image<Gray, Byte> gaussian = source.ToImage<Gray, Byte>().SmoothGaussian(matrix);
            var canny = gaussian.Canny(thresh, threshlinking);
            return canny.ToBitmap();
        }

        public Bitmap GaussianBlur(int matrix, Bitmap source)
        {
            Image<Gray, Byte> image = source.ToImage<Gray, Byte>();
            var image2 = image.SmoothGaussian(matrix);
            return image2.ToBitmap();
        }

        public Bitmap MatToBitmap(Mat matframe)
        {
            return matframe.ToBitmap();
        }

        internal Bitmap BitmapToGrayBitmap(Bitmap source)
        {
            return source.ToImage<Gray, Byte>().ToBitmap();
        }

        internal Bitmap MatToHSVBitmap(int HueMax, int HueMin, int SatMax, int SatMin, int ValueMax, int ValueMin, Mat matframe)
        {
            var hsvframe = matframe.ToImage<Hsv, byte>();

            Hsv lowerLimit = new Hsv(HueMin, SatMin, ValueMin);
            Hsv upperLimit = new Hsv(HueMax, SatMax, ValueMax);

            var thrasholdframe = hsvframe.InRange(lowerLimit, upperLimit);
            var test = thrasholdframe.Erode(1);
            var test2 = test.Dilate(5);

            //summedframe = thrasholdframe.Convert<Bgr, byte>() & matframe.ToImage<Bgr, byte>();
            //var summedframe = ImageComebine(matframe.ToImage<Bgr, byte>(), thrasholdframe.Convert<Bgr, byte>());
            return thrasholdframe.ToBitmap();
        }

        internal Bitmap ImageComebine(Bitmap org, Bitmap mask) //useing bitwise to add Thrashold to ORG image
        {
            var finalimage = org.ToImage<Bgr, byte>() & mask.ToImage<Bgr, byte>();
            return finalimage.ToBitmap(); ;
        }

        internal Bitmap BackgroundSubtraction(Bitmap org, int thrashold)
        {
            if (backgroundImage == null)
            {
                backgroundImage = org.ToImage<Gray, byte>();
            }
            Gray lowerLimit = new Gray(thrashold);
            Gray upperLimit = new Gray(255);

            var diffrence = backgroundImage.AbsDiff(org.ToImage<Gray, byte>());
            var imagethrashold = diffrence.InRange(lowerLimit, upperLimit);
            //var test = imagethrashold.MorphologyEx(MorphOp.Open, imagethrashold, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
            var test = imagethrashold.Erode(3);
            var test2 = test.Dilate(5);
            return test.ToBitmap();
        }

        internal Bitmap AdaptableBackgroundSubtraction(Bitmap org, int history = 2, bool detectShadows = true, int threshold = 16)
        {
            Image<Bgr, byte> imageorg = org.ToImage<Bgr, byte>();
            var forgroundmask = imageorg;
            var mDetector = new BackgroundSubtractorMOG2(history, threshold, detectShadows);

            mDetector.Apply(imageorg, forgroundmask, 0.5);
            return forgroundmask.ToBitmap();
        }
    }
}