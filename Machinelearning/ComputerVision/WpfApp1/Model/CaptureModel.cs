﻿using Emgu.CV;

namespace WpfApp1.Model
{
    internal class CaptureModel
    {
        #region Public Constructors

        public CaptureModel()
        {
        }

        #endregion Public Constructors

        #region Public Properties

        public bool Isrunning
        {
            get;
            set;
        }

        public Mat MatCapturedFrame
        {
            get;
            set;
        }

        public VideoCapture VideoCaptureDevice
        {
            get;
            set;
        }

        #endregion Public Properties
    }
}