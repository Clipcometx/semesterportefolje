﻿using System.Windows;
using WpfApp1.ViewModel;

namespace WpfApp1.View
{
    /// <summary>
    /// Interaction logic for MainPageView.xaml
    /// </summary>
    public partial class MainPageView : Window
    {
        private MainPageViewModel viewModel = new MainPageViewModel();

        public MainPageView()
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}