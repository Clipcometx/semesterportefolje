﻿using Emgu.CV;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using WpfApp1.Model;

namespace WpfApp1.ViewModel
{
    internal class MainPageViewModel : INotifyPropertyChanged
    {
        #region Private Fields

        private BitmapImage buttonImage;
        private BitmapImage buttonImage1;
        private BitmapImage buttonImage2;
        private BitmapImage buttonImage3;
        private BitmapImage buttonImage4;
        private BitmapImage buttonImage5;
        private BitmapImage buttonImage6;
        private BitmapImage buttonImage7;
        private BitmapImage buttonImage8;
        private CaptureModel captureModel = new CaptureModel();
        private int gaussianMatrix = 5;
        private double highThresh = 60;
        private ImageProcceingModel imageProcceingModel = new ImageProcceingModel();
        private double lowThresh = 100;
        private int valueMax = 255;
        private int valueMin = 0;
        private int satMin = 0;
        private int satMax = 255;
        private int hueMin = 0;
        private int hueMax = 255;
        private int backgroundTresh = 10;

        #endregion Private Fields

        #region Public Properties

        public BitmapImage ButtonImage
        {
            get { return buttonImage; }
            set { buttonImage = value; OnPropertyChanged(); }
        }

        public BitmapImage ButtonImage1
        {
            get { return buttonImage1; }
            set { buttonImage1 = value; OnPropertyChanged(); }
        }

        public BitmapImage ButtonImage2
        {
            get { return buttonImage2; }
            set { buttonImage2 = value; OnPropertyChanged(); }
        }

        public BitmapImage ButtonImage3
        {
            get { return buttonImage3; }
            set { buttonImage3 = value; OnPropertyChanged(); }
        }

        public BitmapImage ButtonImage4
        {
            get { return buttonImage4; }
            set { buttonImage4 = value; OnPropertyChanged(); }
        }

        public BitmapImage ButtonImage5
        {
            get { return buttonImage5; }
            set { buttonImage5 = value; OnPropertyChanged(); }
        }

        public BitmapImage ButtonImage6
        {
            get { return buttonImage6; }
            set { buttonImage6 = value; OnPropertyChanged(); }
        }

        public BitmapImage ButtonImage7
        {
            get { return buttonImage7; }
            set { buttonImage7 = value; OnPropertyChanged(); }
        }

        public BitmapImage ButtonImage8
        {
            get { return buttonImage8; }
            set { buttonImage8 = value; OnPropertyChanged(); }
        }

        public int GaussianMatrix
        {
            get { return gaussianMatrix; }
            set { gaussianMatrix = value; OnPropertyChanged(); }
        }

        public double HighThresh
        {
            get { return highThresh; }
            set { highThresh = value; OnPropertyChanged(); }
        }

        public double LowThresh
        {
            get { return lowThresh; }
            set { lowThresh = value; OnPropertyChanged(); }
        }

        public int HueMax
        {
            get { return hueMax; }
            set { hueMax = value; OnPropertyChanged(); }
        }

        public int HueMin
        {
            get { return hueMin; }
            set { hueMin = value; OnPropertyChanged(); }
        }

        public int SatMax
        {
            get { return satMax; }
            set { satMax = value; OnPropertyChanged(); }
        }

        public int SatMin
        {
            get { return satMin; }
            set { satMin = value; OnPropertyChanged(); }
        }

        public int ValueMin
        {
            get { return valueMin; }
            set { valueMin = value; OnPropertyChanged(); }
        }

        public int ValueMax
        {
            get { return valueMax; }
            set { valueMax = value; OnPropertyChanged(); }
        }

        public int BackgroundTresh
        {
            get { return backgroundTresh; }
            set { backgroundTresh = value; OnPropertyChanged(); }
        }

        #endregion Public Properties

        #region Public Constructors

        public MainPageViewModel()
        {
            WebCamStart();
        }

        #endregion Public Constructors

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Public Events

        #region Public Methods

        public void WebCamDispose()
        {
            captureModel.VideoCaptureDevice.Dispose();
        }

        public void WebCamrestart()
        {
            WebCamDispose();
            WebCamStart();
        }

        public void WebCamStart()
        {
            if (captureModel.VideoCaptureDevice == null)
            {
                //captureModel.VideoCaptureDevice = new VideoCapture("rtsp://192.168.8.192:8080/h264_ulaw.sdp");
                captureModel.VideoCaptureDevice = new VideoCapture(1);
            }
            captureModel.VideoCaptureDevice.ImageGrabbed += Webcam_ImageGrabbed;
            captureModel.VideoCaptureDevice.Start();
        }

        public void WebCamStop()
        {
            captureModel.VideoCaptureDevice.Stop();
            captureModel.VideoCaptureDevice.Dispose();
        }

        #endregion Public Methods

        #region Private Methods

        private ImageSource ImageSourceFromBitmap(Bitmap bmp) // takes a Bitmap and turns it into a image source
        {
            try
            {
                return Imaging.CreateBitmapSourceFromHBitmap(bmp.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void MatToProcceing(Mat matframe)
        {
            Bitmap Bitmapframe = matframe.ToBitmap();
            Bitmap Bitmapframe1 = (Bitmap)imageProcceingModel.CannyEdgeDetection(matframe, lowThresh, highThresh, GaussianMatrix).Clone();
            Bitmap Bitmapframe2 = (Bitmap)imageProcceingModel.GaussianBlur(GaussianMatrix, matframe.ToBitmap()).Clone();
            Bitmap Bitmapframe3 = imageProcceingModel.MatToHSVBitmap(hueMax, hueMin, satMax, satMin, valueMax, valueMin, matframe);
            Bitmap Bitmapframe4 = imageProcceingModel.ImageComebine(matframe.ToBitmap(), Bitmapframe3);
            Bitmap Bitmapframe5 = imageProcceingModel.BackgroundSubtraction(matframe.ToBitmap(), backgroundTresh);
            Bitmap Bitmapframe6 = imageProcceingModel.AdaptableBackgroundSubtraction(matframe.ToBitmap());

            SendBitmapToUI(Bitmapframe, 0);  // Orginal
            SendBitmapToUI(Bitmapframe1, 1); // Canny
            SendBitmapToUI(Bitmapframe2, 2); // GaussianBlur
            SendBitmapToUI(Bitmapframe3, 3); // HSV
            SendBitmapToUI(Bitmapframe4, 4); // HSV mapped with org image
            SendBitmapToUI(Bitmapframe5, 5); // Forground Background test
            SendBitmapToUI(Bitmapframe6, 6);
        }

        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void SendBitmapToUI(Bitmap Bitmapframe, int pictureposision)
        {
            BitmapImage source = new BitmapImage();
            using (MemoryStream stream = new MemoryStream())
            {
                Bitmapframe.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);   // Saves the bitmap as a stream
                source.BeginInit();
                source.StreamSource = stream;
                source.CacheOption = BitmapCacheOption.OnLoad;                      // step to prevent memmory leak
                source.EndInit();
                source.Freeze();                                                    // avoid cross thread operations and prevents leaks
            }

            switch (pictureposision)
            {
                case 0:
                    ButtonImage = source;
                    break;

                case 1:
                    ButtonImage1 = source;
                    break;

                case 2:
                    ButtonImage2 = source;
                    break;

                case 3:
                    ButtonImage3 = source;
                    break;

                case 4:
                    ButtonImage4 = source;
                    break;

                case 5:
                    ButtonImage5 = source;
                    break;

                case 6:
                    ButtonImage6 = source;
                    break;

                case 7:
                    ButtonImage7 = source;
                    break;

                case 8:
                    ButtonImage8 = source;
                    break;

                default:
                    break;
            }
        }

        private void Webcam_ImageGrabbed(object sender, EventArgs e) // most of what happens here is to prevent memmory leaks
        {
            using (Mat matframe = new Mat())
            {
                try
                {
                    captureModel.VideoCaptureDevice.Retrieve(matframe, 0);                               // grapping a frame the stream
                    MatToProcceing(matframe);
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion Private Methods
    }
}